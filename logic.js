/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 /* global getAssetRegistry */

 'use strict';
 const namespace = 'org.healthcare.network';
 /**
  * verify the calims
  * @param {org.healthcare.network.verification} verify claims
  * @transaction
  */

 async function onVerification(tx) { // eslint-disable-line no-unused-vars

   const bill_amount = 100;
   console.log('### onVerification' + tx.toString());
   const Patient = await getParticipantRegistry(namespace + '.Patient');
   tx.claim_amount = tx.pId.claim_amount;
   if (tx.claim_amount == bill_amount) {
     tx.verId.approvalStatus = true;
     const registry = await getAssetRegistry('org.healthcare.network.verify');
     insurance(tx);
     await registry.update(tx.verId);
     //insurance(tx);
   } else {
     throw new Error("unsuccessful transaction" + "  " + "claim_amount doesn't match bill_amount");
   }
 }

 async function insurance(tx) {

   const oldvalue = tx.verId.value;
   tx.verId.value = tx.pId.claim_amount;
   const assetRegistry = await getAssetRegistry('org.healthcare.network.verify');
   await assetRegistry.update(tx.ver);
   let event = getFactory().newEvent('org.healthcare.network', 'claimGrant');
   event.verId = tx.verId;
   event.oldValue = oldValue;
   event.newValue = tx.pId.claim_amount;
   emit(event);

 }
